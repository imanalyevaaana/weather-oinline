import React, { useEffect, useState } from "react";

let baseUrl = "https://api.openweathermap.org";

let api = {
	w: "/data/2.5/weather?q=",
};

let key = "d40344464c755b8161dd3ca9bb2ee53a";

const App = () => {
	const [city, setCity] = useState(null);
	const [cityName, setCityName] = useState(null);
	const text = useState("Lorem");
	const fetchWeather = async (name = "London") => {
		const res = await fetch(baseUrl + api.w + name + "&appid=" + key);
		const serverData = await res.json();
		console.log(serverData);
		setCity(serverData);
	};

	const searchCity = () => {
		fetchWeather(cityName.trim());
	};

	const changeBgImg = status => {
		switch (status) {
			case "Clouds":
				return `https://jooinn.com/images/cloudy-72.jpg`;
			case "Rainy":
				return `https://www.holdenbeachvacations.com/blog/wp-content/uploads/2022/07/rainy-day-activities.jpg`;
			case "Clear":
				return `https://img4.goodfon.com/wallpaper/nbig/e/c5/priroda-oblaka-solnyshko-iasnaia-pogoda.jpg`;
			case "Haze":
				return `https://get.pxhere.com/photo/tree-nature-forest-fog-mist-sunlight-morning-dawn-atmosphere-weather-haze-darkness-woodland-habitat-freezing-alsace-drizzle-natural-environment-atmospheric-phenomenon-482952.jpg`;
			default:
				break;
		}
	};

	const errorImg = item => {
		switch (item) {
			case "":
				return `https://www.falcon-cctv.com/include/images/unnamed.jpg `;

			default:
				break;
		}
	};

	useEffect(() => {
		fetchWeather();
	}, []);

	if (!city) return <h1>Loading....</h1>;

	return (
		<div
			style={{
				width: "100%",
				height: "100vh",
				margin: "0 auto",
				backgroundImage: `url(${changeBgImg(city.weather[0].main)})`,
				position: "relative",
			}}>
			<div
				style={{
					color: "white",
					display: "flex",
					alignItems: "center",
					gap: 2,
					position: "absolute",
					bottom: 50,
					left: 80,
					padding: "10px",
					fontFamily: "sans-serif",
				}}>
				<div>
					<h2>{Math.round(city.main.temp - 273.15)} °C</h2>
				</div>
				<div>
					<h6>{city.name}</h6>
					<p>06:09 Sunday 6 Oct 23</p>
				</div>
				<div style={{ textAlign: "center" }}>
					<img
						src={`http://openweathermap.org/img/wn/${city.weather[0].icon}.png`}
						alt=""
					/>
					<p>{city.weather[0].main}</p>
				</div>
			</div>
			<div
				className="block"
				style={{
					position: "absolute",
					right: 0,
					height: "100%",
					padding: "20px",
					fontFamily: "sans-serif",
					backdropFilter: "blur(15px)",
				}}>
				<input
					style={{
						padding: "5px",
						background: "none",
						border: "2px solid",
						borderRadius: "8px",
					}}
					onChange={event => setCityName(event.target.value)}
					type="text"
					placeholder="Another location"
				/>
				<button className="btn btn-outline-warning" onClick={searchCity}>
					<i class="bi bi-search"></i>
				</button>
				<div>
					<ul>
						<li>Birmingham</li>
						<li>Manchester</li>
						<li>New York</li>
						<li>California</li>
					</ul>
				</div>
				<div style={{ marginTop: "100px" }}>
					<h5>Weather Details</h5>
					<div className="d-flex js-between">
						<p>Clouds</p>
						<p>{city.clouds.all}%</p>
					</div>
					<div className="d-flex js-between">
						<p>Humidity</p>
						<p>{city.main.humidity}</p>
					</div>
					<div className="d-flex js-between">
						<p>Wind</p>
						<p>{city.wind.speed}km/h</p>
					</div>
					<div className="d-flex js-between">
						<p>Rain</p>
						<p>8 mm</p>
					</div>
				</div>
			</div>
		</div>
	);
};
export default App;
